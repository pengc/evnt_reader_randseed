//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Nov  7 15:50:28 2019 by ROOT version 6.08/06
// from TTree CollectionTree/CollectionTree
// found on file: EVNT.12242854._000001.pool.root.1
//////////////////////////////////////////////////////////

#ifndef MyClass_h
#define MyClass_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
//#include "EventInfo_p4.h"
//#include "McEventCollection_p5.h"

class Derived_GenEvent_p5 : public GenEvent_p5{
 public :
  int Derive_eventNbr(){
    return m_eventNbr;
  }
  std::vector<long int> Derive_randomStates(){
    return m_randomStates;
  }
};

class Derived_McEventCollection_p5 : public McEventCollection_p5{
 public :
  std::vector<GenEvent_p5> Derive_GenEvent_p5(){
    return m_genEvents;
  }
};


class MyClass {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   EventInfo_p4    *EventInfo_p4_McEventInfo;
   Derived_McEventCollection_p5 *McEventCollection_p5_GEN_EVENT;

   // List of branches
   TBranch        *b_EventInfo_p4_McEventInfo;   //!
   TBranch        *b_McEventCollection_p5_GEN_EVENT;   //!

   MyClass(TTree *tree=0);
   virtual ~MyClass();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MyClass_cxx
MyClass::MyClass(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("EVNT.12242854._000001.pool.root.1");
      if (!f || !f->IsOpen()) {
         f = new TFile("EVNT.12242854._000001.pool.root.1");
      }
      f->GetObject("CollectionTree",tree);

   }
   Init(tree);
}

MyClass::~MyClass()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MyClass::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MyClass::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MyClass::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   EventInfo_p4_McEventInfo = 0;
   McEventCollection_p5_GEN_EVENT = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventInfo_p4_McEventInfo", &EventInfo_p4_McEventInfo, &b_EventInfo_p4_McEventInfo);
   fChain->SetBranchAddress("McEventCollection_p5_GEN_EVENT", &McEventCollection_p5_GEN_EVENT, &b_McEventCollection_p5_GEN_EVENT);
   Notify();
}

Bool_t MyClass::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MyClass::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MyClass::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MyClass_cxx
